#!/usr/bin/env python3
import argparse
import logging
import os
from prometheus_client import start_http_server, Gauge
import requests
import time
import platform

# Create a metric to track time spent and requests made.
DOCKERHUB_LIMIT = Gauge("dockerhub_limit_limit", "Total API limit", ["user", "host"])
DOCKERHUB_CURRENT = Gauge(
    "dockerhub_limit_current", "Remaining API Usage", ["user", "host"]
)


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(
        description="This is my super sweet script",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )

    return parser.parse_args()


def process_limits(update_increment):
    logging.info("Checking current limits")
    params = {
        "service": "registry.docker.io",
        "scope": "repository:ratelimitpreview/test:pull",
    }
    dockerhub_username = os.environ.get("DOCKERHUB_USERNAME")
    dockerhub_password = os.environ.get("DOCKERHUB_PASSWORD")

    host = os.environ.get("DOCKERHUB_LOCAL_HOST", platform.node())

    use_auth = False
    if dockerhub_username and dockerhub_password:
        use_auth = True

    if use_auth:
        logging.debug("Using authenticated token request")
        auth = (dockerhub_username, dockerhub_password)
        user = dockerhub_username
    else:
        logging.debug("Using anonymouse token request")
        user = "anonymous"
        auth = None
    token_request = requests.get(
        "https://auth.docker.io/token", auth=auth, params=params,
    )
    token = token_request.json()["token"]
    headers = {"Authorization": "Bearer %s" % token}

    limit_request = requests.head(
        "https://registry-1.docker.io/v2/ratelimitpreview/test/manifests/latest",
        headers=headers,
    )
    limit_raw = limit_request.headers["RateLimit-Limit"]
    current_raw = limit_request.headers["RateLimit-Remaining"]
    limit, limit_window_raw = limit_raw.split(";")
    current, current_window_raw = current_raw.split(";")
    # limit_window = limit_window_raw.split("=")[1]
    # current_window = current_window_raw.split("=")[1]
    DOCKERHUB_LIMIT.labels(user=user, host=host).set(limit)
    DOCKERHUB_CURRENT.labels(user=user, host=host).set(current)
    time.sleep(update_increment)


if __name__ == "__main__":
    # Start up the server to expose the metrics.
    args = parse_args()
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

    start_http_server(8000)
    update_increment = int(os.environ.get("DOCKERHUB_UPDATE_INCREMENT", 60))
    while True:
        process_limits(update_increment=update_increment)
