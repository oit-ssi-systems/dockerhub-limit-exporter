# dockerhub-limit-exporter for Prometheus

This app exports Dockerhub limit and usage as a Prometheus exporter

## Configuration

By default, anonymous limits are collected.  If you would like to use an
account for authentication, set the following environment variables:

`DOCKERHUB_USERNAME`
`DOCKERHUB_PASSWORD`

If you would like to override the auto-detected hostname, set:

`DOCKERHUB_LOCAL_HOST`
