FROM python:3.8

RUN mkdir /code
EXPOSE 8000
COPY . /code
RUN pip install -r /code/requirements.txt

WORKDIR /code
CMD ["python","app.py"]
